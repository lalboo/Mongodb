//引入包
const express=require('express');
const expressStatic=require('express-static');
const slideRouter=require('./routes/slide');
const userRouter=require('./routes/user');
const dbConnect=require('./utils/db/mongdbConnect');
const bodyParser=require('body-parser');
const cookieParser=require('cookie-parser');
const cookieSession=require('cookie-session');
const viewRouter=require('./routes/views');
const childRouter=require('./routes/child');
const womenRouter=require('./routes/she');
const menRouter=require('./routes/he');
const mobRouter=require('./routes/mob');
const mainRouter=require('./routes/main');
//创建服务
var app=express();

app.listen(8080,function(err){
    if(err){
        console.log('App is error!!!')
    }else{
        console.log('App is running!!!!')
    }
})
//开启数据连接
dbConnect.dbConnect();
//启用body-parser
app.use(bodyParser.urlencoded({extend:true}));
//启用cookie
var keys=[];
for(var i=0;i<100;i++){
    var str='abcd133%%^&'+Math.random(1000000);
    keys.push(str);
}
app.use(cookieParser());
app.use(cookieSession({
    name:'storeOnline',
    keys:keys,
    maxAge:20*60*1000
}));

//启用路由
app.use('/slide',slideRouter.router);
app.use('/user',userRouter.router);
app.use('/views',viewRouter.router);
app.use('/child',childRouter.router);
app.use('/she',womenRouter.router);
app.use('/he',menRouter.router);
app.use('/mob',mobRouter.router);
app.use('/main',mainRouter.router);
//启动页面
app.use('/',function(req,res,next){
    var options={
        root:__dirname+'/www/',
        dotfiles:'deny',
        headers:{
            'x-timestamp':Date.now(),
            'x-sent':true
        }
    }
    var url=req.url;
    if(url.indexOf('/')==0&&url.length>1){
        next();
    }else{
        res.sendFile('index.html',options,function(err){
            if(err){
                console.log('index html id error!!')
            }
        })
    }

})
//静态页面访问
app.use(expressStatic('./www'));
