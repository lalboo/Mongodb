/**
 * Created by Mo on 2020/6/19.
 */
const express=require('express');
const userService=require('../services/userService');
const cookieParser=require('cookie-parser');
const cookieSession=require('cookie-session');

var router=express.Router();

router.post('/register',async(req,res,next)=>{
    var user=req.body;
    console.log('fasjhdj');
    console.log(user);
    if(user==undefined||user=={}||user==[]){
        res.send({success:false,msg:'用户名与密码不能为空'});
    }else{
        var data=await userService.addUser(user);
        if(data.success){
            res.send({success:true,user:data.user});
        }else {
            res.send({success:false,msg:data.msg});
        }
    }
});
//登录
router.post('/login',async(req,res,next)=>{
    var user=req.body;
    //var cookies=req.cookies;
    if(user==undefined||user=={}||user==[]){
        res.send({success:false,msg:'用户名与密码不能为空'});
    }else{
      var data=await userService.login(user);
        //登录成功要进行session与cookie
        if(data.success) {
            //登录成功，对cookie、session操作
            if (user.remember=='true') {
                res.cookie("user", data.user, {maxAge: 7 * 24 * 60 * 60 * 1000});
            }
            //登录之后立即记录
                req.session.user= data.user;
        };
        res.send(data);
    }
});
//用户退出时 清除session
router.use('/logout',(req,res)=>{
    var user=req.session['user'];
    //console.log('logout success!');
    if(user==null||user=={}||user==undefined){
        res.send('Havn\'t login in!!!')
    }else{
        req.session.destory();
        res.send('/views/index.html');
    }
})

exports.router=router;