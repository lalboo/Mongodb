/**
 * Created by Mo on 2020/6/22.
 */
const express=require('express');
const wearService=require('../services/wearService');


var router=express.Router();

router.use('/mens',async(req,res,next)=>{
    //找出所有的kid wear
    var GET=req.query;
    var kind=GET.kind;
    if(kind==undefined||kind==null||kind=={}||kind==[]){
        kind='men';
    }
    var data=await wearService.findByKind(kind);
    var img=[];
    for(var i=0;i<5;i++){
        img.push(data.wears[i]);
    }
    res.send({success:true,wears:img});
    //res.send(data);
})

router.use('/womens',async(req,res,next)=>{
    //找出所有的kid wear
    var GET=req.query;
    var kind=GET.kind;
    if(kind==undefined||kind==null||kind=={}||kind==[]){
        kind='women';
    }
    var data=await wearService.findByKind(kind);
    var img=[];
    for(var i=0;i<5;i++){
        img.push(data.wears[i]);
    }
    res.send({success:true,wears:img});
    //res.send(data);
})

router.use('/kids',async(req,res,next)=>{
    //找出所有的kid wear
    var GET=req.query;
    var kind=GET.kind;
    if(kind==undefined||kind==null||kind=={}||kind==[]){
        kind='kid';
    }
    var data=await wearService.findByKind(kind);
    var img=[];
    for(var i=0;i<5;i++){
        img.push(data.wears[i]);
    }
    res.send({success:true,wears:img});
    //res.send(data);
})

router.use('/mobiles',async(req,res,next)=>{
    //找出所有的kid wear
    var GET=req.query;
    var kind=GET.kind;
    if(kind==undefined||kind==null||kind=={}||kind==[]){
        kind='mob';
    }
    var data=await wearService.findByKind(kind);
    var img=[];
    for(var i=0;i<5;i++){
        img.push(data.wears[i]);
    }
    res.send({success:true,wears:img});
    //res.send(data);
})
exports.router=router;