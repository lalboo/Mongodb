/**
 * Created by Mo on 2020/6/19.
 */
const express=require('express');
const cookieParser=require('cookie-parser');
const cookieSession=require('cookie-session');
const userService=require('../services/userService');

var router=express.Router();

//登录和自动登录
router.use('/loginPage.html',async function(req,res,next){
    var user=req.cookies.user;
    if(user=={}||user==undefined){
        next();
    }else if(user.role=='admin'){
        console.log('role admin is not auto login');
        next();
    }else {
         //自动登录
        var data=await userService.loginCookie(user);
        if(data.success){
            req.session.user=data.user;
            res.redirect('/views/pages/user1.html');
        }else{
            next();
        }
    };
});
//对pages里的东西进行控制
router.use('/pages',function(req,res,next){
    var user=req.session.user;
    if(user==[]||user=={}||user==undefined||user==''){
        res.redirect('/views/loginPage.html');
    }else{
        next();
    }
});

router.use('/admin',function(req,res,next){
    var user=req.session.user;
    if(user==[]||user=={}||user==undefined||user==''){
        res.redirect('/views/loginPage.html');
    }else if(user.role=='user'){
        res.redirect('/views/loginPage.html')
    }else{
        next();
    }
})

////在views路由上做登录控制
//router.use('/pages',(req,res,next)=>{
//    var user=req.session['user'];
//    if(user==null||user=={}||user==undefined){
//        res.redirect('/views/loginPage.html');
//    }else{
//        next();
//    }
//})
exports.router=router;