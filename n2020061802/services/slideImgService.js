/**
 * Created by Mo on 2020/6/19.
 */
const slideImgDao=require('../dao/slideImgDao');

async function findAllSlideImgs(){
    var data=await slideImgDao.findAllSlideImgs();
    //var imgsrcs=[];
    //for(var i=0;i<data.length;i++){
    //    imgsrcs.push(data[i].imgsrc);
    //}
    return data;
}

exports.findAllSlideImgs=findAllSlideImgs;