/**
 * Created by Mo on 2020/6/19.
 */
const userDao=require('../dao//userDao');
const md5=require('md5');
const toMd5=require('../utils/md5/md5');
//1、注册：查找用户的存在性，否则增加
async function addUser(user){
    var ret=await userDao.findByName(user.name);
    if(ret.success){
        return{success:false,msg:'用户已存在!'};
    }else{
        user.password=toMd5.toMd5(user.password);
        console.log(user);
        var user=await userDao.addUser(user);
        //return user;
        return{success:true,user:user};
    }
}

//2、登录：找出用户，然后进行密码比对
//对数据库里的密码进行md5验证，所以数据一般要进行密码验证
async function login(user){
    var ret=await userDao.findByName(user.name);
    if(ret.success){
        var password=toMd5.toMd5(user.password);
        if(password==ret.user.password){
            return{success:true,user:ret.user};
        }else{
            return{success:false,msg:'用户密码不正确'}
        }
    }else{
        return{success:false,msg:'用户名不存在'}
    }
}
async function loginCookie(user){
    var ret=await userDao.findByName(user.name);
    console.log(user);
    if(ret.success){
        //var password=toMd5.toMd5(user.password);
        if(user.password==ret.user.password){
            return{success:true,user:ret.user};
        }else{
            return{success:false,msg:'用户密码不正确'}
        }
    }else{
        return{success:false,msg:'用户名不存在'}
    }
}
exports.addUser=addUser;
exports.login=login;
exports.loginCookie=loginCookie;