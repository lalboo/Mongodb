/**
 * Created by Mo on 2020/6/19.
 */
const mongoose=require('mongoose');
//1 schema
var schema=mongoose.Schema({
    _id:Object,
    imgsrc:String

});
//2 model
var slideImgModel=mongoose.model('slide',schema);

exports.slideImgModel=slideImgModel;